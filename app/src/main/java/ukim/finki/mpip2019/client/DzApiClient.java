package ukim.finki.mpip2019.client;

import retrofit2.Retrofit;

import retrofit2.converter.gson.GsonConverterFactory;
import ukim.finki.mpip2019.service.DzPlaylistService;

public class DzApiClient {

    private static Retrofit retrofit = null;

    private static Retrofit getRetrofit() {
        if(retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl("https://api.deezer.com/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

    public static DzPlaylistService getService() {
        return getRetrofit().create(DzPlaylistService.class);
    }

}
