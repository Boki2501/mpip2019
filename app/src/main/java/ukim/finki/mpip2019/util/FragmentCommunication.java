package ukim.finki.mpip2019.util;

public interface FragmentCommunication {

    void onItemFromFragmentSelected(String item);

}
