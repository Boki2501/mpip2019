package ukim.finki.mpip2019.viewmodels;

import android.app.Application;
import android.arch.lifecycle.AndroidViewModel;
import android.arch.lifecycle.LiveData;
import ukim.finki.mpip2019.asynctask.DzPlaylistAsyncTask;
import ukim.finki.mpip2019.db.Repository;
import ukim.finki.mpip2019.models.DzTrack;

import java.util.List;

public class PlaylistViewModel extends AndroidViewModel {

    Repository repository;

    public PlaylistViewModel(Application application) {
        super(application);
        repository = new Repository(application);
        fetchData();
    }

    public LiveData<List<DzTrack>> getAll() {
        return repository.getAllTracks();
    }

    private void fetchData() {
        DzPlaylistAsyncTask asyncTask = new DzPlaylistAsyncTask(repository);
        asyncTask.execute(1867419722L);
    }

}
