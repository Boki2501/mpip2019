package ukim.finki.mpip2019;

import ukim.finki.mpip2019.models.DzPlaylist;

public interface PlaylistInterface {

    void loadedDzPlaylist(DzPlaylist playlist);

}
