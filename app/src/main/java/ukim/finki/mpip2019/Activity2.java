package ukim.finki.mpip2019;

import android.arch.lifecycle.ViewModelProviders;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.support.v7.widget.Toolbar;
import android.support.v7.widget.SearchView;
import ukim.finki.mpip2019.adapters.CustomListAdapter;
import ukim.finki.mpip2019.broadcast.AirplaneModeBroadcastReceiver;
import ukim.finki.mpip2019.holders.CustomListViewHolder;
import ukim.finki.mpip2019.viewmodels.PlaylistViewModel;
import java.util.logging.Logger;

import static android.provider.AlarmClock.*;


public class Activity2 extends AppCompatActivity {

    private Button button2;
    CustomListAdapter adapter;

    Logger logger = Logger.getLogger("Activity2");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_2);
        initToolbar();
        initListView();
//        initData();

        SharedPreferences preferences = this.getSharedPreferences("ukim.finki.mpip2019.SHARED_PREF", Context.MODE_PRIVATE);
        String mode = preferences.getString("mode", "light");

        logger.info("SHARED_PREF: " + mode);
    }

    public void initData() {
        PlaylistViewModel playlistViewModel = ViewModelProviders.of(this).get(PlaylistViewModel.class);

        playlistViewModel.getAll().observe(this, data -> {
            adapter.updateDataset(data);

            sendBroadcast(new Intent(Intent.ACTION_PROCESS_TEXT));
        });
    }

    private void initToolbar() {
        Toolbar toolbar = findViewById(R.id.custom_toolbar);
        setSupportActionBar(toolbar);
    }

    private void initListView() {
        RecyclerView recyclerView =  findViewById(R.id.recycler_view_1);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new CustomListAdapter(getItemViewOnClickListener());
        recyclerView.setAdapter(adapter);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.custom_menu, menu);

        SearchView searchView = (SearchView) menu.findItem(R.id.menu_item1).getActionView();

        searchView.setOnQueryTextListener(getOnQueryTextListener());
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case R.id.menu_item1:
                ///
                logger.info("Clicked menu item: 1");
                break;
            case R.id.menu_item2:
                ///
                logger.info("Clicked menu item: 2");
                break;
            case R.id.menu_item3:
                //
                logger.info("Clicked menu item: 3");
                break;
        }
        return super.onOptionsItemSelected(item);
    }

//    public void insertElementIntoList(String element) {
//        dataset.add(element);
//        adapter.notifyDataSetChanged();
//    }

    private View.OnClickListener getItemViewOnClickListener() {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CustomListViewHolder holder = (CustomListViewHolder) v.getTag();
                Long selectedTrackId = adapter.getClickedItemId(holder);


            }
        };
    }

    private SearchView.OnQueryTextListener getOnQueryTextListener() {
        return new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                logger.info("Query text change: " + newText);
                return false;
            }
        };

    }

//    void initViews() {
//        button2 = findViewById(R.id.button2);
//    }

//    void initListeners() {
//        button2.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                callImplicitActivity();
//            }
//        });
//    }

    void callImplicitCustomActivity() {
        Intent customActionIntent = new Intent();

        customActionIntent.setAction("ukim.finki.mpip2019.CUSTOM_ACTION");
        customActionIntent.addCategory("android.intent.category.DEFAULT");

        startActivity(customActionIntent);
    }

    void callImplicitActivity() {
        Intent implicitIntent = new Intent();

        implicitIntent.setAction(ACTION_SET_ALARM);
        implicitIntent.putExtra(EXTRA_HOUR, 6);
        implicitIntent.putExtra(EXTRA_MINUTES, 10);
        implicitIntent.setType("text/plain");
        implicitIntent.addCategory("android.intent.category.DEFAULT");

        Intent chooser = Intent.createChooser(implicitIntent, "Choose the desired app!");

        if(chooser.resolveActivity(getPackageManager()) != null) {
            startActivity(chooser);
        }
    }
}
