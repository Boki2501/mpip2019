package ukim.finki.mpip2019;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import ukim.finki.mpip2019.broadcast.AirplaneModeBroadcastReceiver;
import ukim.finki.mpip2019.fragments.DemoFragment;
import ukim.finki.mpip2019.util.FragmentCommunication;

import java.util.logging.Logger;


public class MainActivity extends AppCompatActivity implements FragmentCommunication {

    private Logger logger = Logger.getLogger("MainActivity");

    private Button button1;

    AirplaneModeBroadcastReceiver receiver;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initViews();
        initListeners();

        logger.info("INFO: ON Create");

        receiver = new AirplaneModeBroadcastReceiver();
        registerReceiver(
                receiver,
                new IntentFilter(Intent.ACTION_AIRPLANE_MODE_CHANGED)
        );

        SharedPreferences preferences = this.getSharedPreferences("ukim.finki.mpip2019.SHARED_PREF", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("mode", "dark");
        editor.apply();

        FragmentManager manager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = manager.beginTransaction();
        fragmentTransaction.add(R.id.fragment_container, new DemoFragment(this));
        fragmentTransaction.commit();

        DemoFragment fragmentById = (DemoFragment)getSupportFragmentManager().findFragmentById(R.id.fragment_container);

    }

    @Override
    protected void onStart() {
        super.onStart();

        logger.info("INFO: ON Start");
    }

    @Override
    protected void onResume() {
        super.onResume();

        logger.info("INFO: ON Resume");
    }

    @Override
    protected void onPause() {
        super.onPause();

        logger.info("INFO: ON Pause");
    }

    @Override
    protected void onStop() {
        super.onStop();
        logger.info("INFO: ON Stop");
        unregisterReceiver(receiver);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        logger.info("INFO: ON Destroy");
    }

    void initViews() {
        button1 = findViewById(R.id.button1);

    }

    void initListeners() {
        button1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                launchActivity();
            }
        });
    }

    void launchActivity() {
        Intent intent = new Intent(this, Activity2.class);

        startActivity(intent);
    }


    @Override
    public void onItemFromFragmentSelected(String item) {
        logger.info("Item selected: " + item);

//        FragmentManager manager = getSupportFragmentManager();
//        FragmentTransaction fragmentTransaction = manager.beginTransaction();
//        fragmentTransaction.replace(R.id.fragment_container, new DemoFragment());
//        fragmentTransaction.addToBackStack("back1");
//        fragmentTransaction.commit();
    }
}
